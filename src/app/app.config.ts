import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideToastr } from 'ngx-toastr';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideHttpClient } from '@angular/common/http';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getDatabase, provideDatabase } from '@angular/fire/database';
import { getStorage, provideStorage } from '@angular/fire/storage';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes),
  provideHttpClient(),
  provideAnimations(), // required animations providers
  provideToastr(), provideFirebaseApp(() => initializeApp({"projectId":"proyectuseless","appId":"1:571381492735:web:41fe321c67ae04f3bbd5ca","storageBucket":"proyectuseless.appspot.com","apiKey":"AIzaSyBOVZ6xemNAYe01c2l9bLxYe6nWqHpoixs","authDomain":"proyectuseless.firebaseapp.com","messagingSenderId":"571381492735"})), provideAuth(() => getAuth()), provideFirestore(() => getFirestore()), provideDatabase(() => getDatabase()), provideStorage(() => getStorage())]
};

