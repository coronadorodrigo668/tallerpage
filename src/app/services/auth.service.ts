import { Injectable, inject } from "@angular/core";
import { Auth, UserCredential, authState, createUserWithEmailAndPassword, signInWithEmailAndPassword, GithubAuthProvider,
    GoogleAuthProvider,  signInWithPopup} from "@angular/fire/auth";
export interface Credential{
    email: string;
    password: string;
}

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private auth: Auth = inject(Auth);
    readonly authState$ = authState(this.auth);

    signUpWithEmailAndPassword(credential: Credential): Promise<UserCredential>{
        return createUserWithEmailAndPassword(this.auth, credential.email, credential.password);
    }

    signInWithEmailAndPassword(credential: Credential): Promise<UserCredential>{
        return signInWithEmailAndPassword(this.auth, credential.email, credential.password);
    }
    logOut(): Promise<void>{
        return this.auth.signOut();
    }


    signInWithGoogleProvider(): Promise<UserCredential> {
        const provider = new GoogleAuthProvider();
    
        return this.callPopUp(provider);
      }
    
      signInWithGithubProvider(): Promise<UserCredential> {
        const provider = new GithubAuthProvider();
    
        return this.callPopUp(provider);
      }
      async callPopUp(provider: any): Promise<UserCredential> {
        try {
          const result = await signInWithPopup(this.auth, provider);
          return result;
        } catch (error: any) {
          return error;
        }
      }
}