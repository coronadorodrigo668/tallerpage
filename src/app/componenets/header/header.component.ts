import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {
  redirectToHome(): void {
    window.location.href = '/browse';
  }
  constructor(@Inject(AuthService) private authService: AuthService, private router: Router) {}

  async logOut(): Promise<void> {
    try {
      await this.authService.logOut();
      this.router.navigateByUrl('/')
    } catch (error) {
      console.error(error);
    }
  }
}
